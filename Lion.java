public class Lion{
	public String originCountry;
	public int maxSpeed;
	public int mass;
	
	public void loudRoar(){
		System.out.println("ROAAARRRRRRRR");
	}
	
	public void goHunt(){
		if(this.maxSpeed < 40 && this.mass > 220){
			System.out.println("This lion seems to not be able to go hunt :(");
		}
		else
			System.out.println("This lion will go hunt and bring food to his pride :)");
	}
}