import java.util.Scanner;
public class VirtualPetApp {
	public static void main (String[] args){
		Lion[] pride = new Lion[4];
		
		Scanner reader = new Scanner(System.in);
		
		for (int i = 0; i < pride.length; i++){
			pride[i] = new Lion();
			
			System.out.println("Enter the origin country of your lion: ");
			pride[i].originCountry = reader.nextLine();
			
			System.out.println("Enter the max speed (in Km/h) of your lion: ");
			pride[i].maxSpeed = Integer.parseInt(reader.nextLine());
			
			System.out.println("Enter the mass (in Kg) of your lion: ");
			pride[i].mass = Integer.parseInt(reader.nextLine());
		}
		
		System.out.println("Here is your last lion's data: ");
		System.out.println("The origin country is: " + pride[3].originCountry);
		System.out.println("The max speed is: " + pride[3].maxSpeed);
		System.out.println("The mass is: " + pride[3].mass);
		
		pride[0].loudRoar();
		pride[0].goHunt();
	}
}